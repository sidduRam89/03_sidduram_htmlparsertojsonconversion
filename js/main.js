const fs = require('fs');
fs.readFile('./index.html', 'utf8', function (err, data) {
    if (err)
        console.error(err);
    else {
        let keys = getKeys(data);
        let values = getValues(data);
        let tableJson = convertTableToJson(keys,values);
        console.log(tableJson);
    }
})
function getValues(data){
    let x = data.match(/<td.*>(\w*)<\/td>/ig);
    let values = [];
    for (var i = 0; i < x.length; i++) {
        let y = x[i].match(/<td.*>(\w*)<\/td>/);
        values.push(y[1]);
    }
    return values;
}
function getKeys(data) {
    let x = data.match(/<th.*>(\w*)<\/th>/ig);
    let keys = [];
    for (var i = 0; i < x.length; i++) {
        let y = x[i].match(/<th.*>(\w*)<\/th>/);
        keys.push(y[1]);
    }
    return keys;
}
function convertTableToJson(keys,values){
    let tableJson = [];
    let temp = {};
    let count =0;
    values.forEach(value =>{
        if(keys.length===count){
            count=0;
            tableJson.push(JSON.stringify(temp));
            temp ={};
        }
        temp[keys[count]]=value;
        ++count;
    });
    return tableJson;
}